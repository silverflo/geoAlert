package com.example.geoalert

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity(), LocationListener {

    private lateinit var locationManager: LocationManager


    private var curLatitude : Double = 0.0
    private var curLongitude : Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val gpsIcon: FloatingActionButton = findViewById(R.id.gpsButton)
        gpsIcon.setOnClickListener(){
            updateLocation()
        }
        val calcBtn: Button = findViewById(R.id.calculateBtn)
        calcBtn.setOnClickListener(){
            calcDistance()
        }
        val countBtn: Button = findViewById(R.id.startTimer)
            countBtn.setOnClickListener(){
               startTimer()
            }

    }

    //        request permission
    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                Log.i("Permission: ", "Granted")
            } else {
                Log.i("Permission: ", "Denied")
            }
        }

    private fun showLocation(){
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {

                // GPS API that requires the permission.
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5f,this)
            }

            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestPermissionLauncher.launch(
                    Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }
    }

//    private fun calcDistance() {
//        Location()
//       var  currentLocation: Location
//       val destinationLocation: Location
//       currentLocation.latitude = latVal.text
//    }

    private fun calcDistance(){
        var current = Location(LocationManager.GPS_PROVIDER)
        var destination = Location(LocationManager.GPS_PROVIDER)



        current.latitude = curLatitude
        current.longitude = curLongitude
        val destLat = findViewById<EditText>(R.id.destLat)
        val destLong = findViewById<EditText>(R.id.destLong)

       /* if (destLat.isEmpty() || destLong.isEmpty()) {
            throw IllegalArgumentException("Invalid initial value")
        }

        */

        destination.latitude = destLat.text.toString().toDouble()
        destination.longitude = destLong.text.toString().toDouble()

        var distance = current.distanceTo(destination)

        val distancetest = findViewById<TextView>(R.id.distance)
        distancetest.text = distance.toString()


    }


    override fun onLocationChanged(p0: Location) {
        val latVal =  findViewById<TextView>(R.id.locVal)
        val longVal = findViewById<TextView>(R.id.locVal2)
        curLatitude = p0.latitude
        curLongitude = p0.longitude
        latVal.text = p0.latitude.toString()
        longVal.text = p0.longitude.toString()

    }

    // count button and inc functions

    private fun startTimer(){
        val myTimer = object : CountDownTimer(30000, 1000) {
            val countview = findViewById<TextView>(R.id.timerText)

            override fun onTick(millisUntilFinished: Long) {
                countview.text = "Remaining: " + millisUntilFinished / 1000
            }

            override fun onFinish() {
                countview.text = "done!"
            }
        }
        myTimer.start()

    }


    // continuos update location

    private fun updateLocation(){
        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                showLocation()
                calcDistance()
            }

            override fun onFinish() {
                //do nothing
            }
        }.start()

    }


}


